package step_definitions;

import cucumber.api.java8.En;
import pageObjectModel.CommonFunctions;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class MyStepdefs implements En {
    public MyStepdefs() throws IOException {
        String imageUrl;
        String sliderUrl;
        String hoverUrl;
        String userName;
        String password;
        CommonFunctions comFns=new CommonFunctions();
        FileReader reader=new FileReader("src/main/java/readData.properties");
        Properties read=new Properties();
        read.load(reader);
        imageUrl=read.getProperty("brokenImagesURL");
        sliderUrl=read.getProperty("moveSliderURL");
        hoverUrl=read.getProperty("hoverURL");
        userName=read.getProperty("userName");
        password=read.getProperty("password");

        Given("^enter the image URL on the browser$", () -> {
           comFns.launchApplication(imageUrl,"CHROME");
        });
        When("^get all the images and assert the broken images$", () -> {
          comFns.verifyImages();
        });
        Then("^quite the browser$", () -> {
           comFns.quiteBrowser();
        });
        Given("^enter the login URL on the browser$", () -> {
           comFns.loginFns(userName,password,"CHROME");
        });
        When("^enter the userName and password$", () -> {
           comFns.verifyBasicAuthSuccessMessage();
        });
        Then("^assert the login success message$", () -> {
           comFns.quiteBrowser();
        });
        Given("^enter the slider URL on the browser$", () -> {
            comFns.launchApplication(sliderUrl,"CHROME");
        });
        Then("^move the slider to max and assert the value$", () -> {
            comFns.moveSliderRight();
            comFns.moveSliderLeft();
        });
        And("^move the slider to min and assert the value$", () -> {
            comFns.quiteBrowser();
        });

        Given("^enter the hover URL on the browser$", () -> {
            comFns.launchApplication(hoverUrl,"CHROME");
        });
        Then("^mouse hover on the images and get the details of the images$", () -> {
            comFns.mouseHoverAndGetDetails();
        });
        Then("^close the browser$", () -> {
            comFns.quiteBrowser();
        });
    }
}
