$(document).ready(function() {
CucumberHTML.timelineItems.pushArray([
  {
    "id": "verify-the-sinnerschrader-functionality;evaluate-the-broken-images-out-of-all-images-and-assert-the-result.",
    "feature": "Verify the sinnerSchrader functionality",
    "scenario": "Evaluate the broken images out of all images and assert the result.",
    "start": 1578491876184,
    "end": 1578491892072,
    "group": 1,
    "content": "",
    "className": "passed",
    "tags": "@basicfeatures,"
  },
  {
    "id": "verify-the-sinnerschrader-functionality;move-slider-to-max-and-min-and-assert-the-value",
    "feature": "Verify the sinnerSchrader functionality",
    "scenario": "Move slider to max and min and assert the value",
    "start": 1578491907786,
    "end": 1578491922154,
    "group": 1,
    "content": "",
    "className": "passed",
    "tags": "@basicfeatures,"
  },
  {
    "id": "verify-the-sinnerschrader-functionality;hover-on-the-pictures-and-assert-the-details-like-user",
    "feature": "Verify the sinnerSchrader functionality",
    "scenario": "Hover on the pictures and assert the details like User",
    "start": 1578491922158,
    "end": 1578491934764,
    "group": 1,
    "content": "",
    "className": "passed",
    "tags": "@basicfeatures,"
  },
  {
    "id": "verify-the-sinnerschrader-functionality;perform-login-with-basic-auth",
    "feature": "Verify the sinnerSchrader functionality",
    "scenario": "Perform login with Basic Auth",
    "start": 1578491892076,
    "end": 1578491907778,
    "group": 1,
    "content": "",
    "className": "passed",
    "tags": "@basicfeatures,"
  }
]);
CucumberHTML.timelineGroups.pushArray([
  {
    "id": 1,
    "content": "Thread[main,5,main]"
  }
]);
});